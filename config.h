#ifndef CONFIG_H__
#define CONFIG_H__

static int instant = 0;
static int topbar = 1;
static int fuzzy = 0;
static int incremental = 0;

static const char *fonts[] = {"Fira Code:size=14", "monospace:size=14"};
static const char *prompt = NULL;
static const char *colors[SchemeLast][2] = {
    [SchemeNorm] = {"#bbbbbb", "#222222"},
    [SchemeSel] = {"#eeeeee", "#005577"},
    [SchemeOut] = {"#000000", "#00ffff"},
};
static unsigned int lines = 0;

static const char worddelimiters[] = " /?\"&[]";
#endif // CONFIG_H__
